import requests
import logging
import re
import datetime
import hashlib

adlists_path = 'adlist_urls.txt'
outfile = 'blacklist.txt'

loglevel = logging.ERROR
logging.basicConfig(filename="blacklist.log", level=loglevel)

hashmap = {}

def get_adlist(url):
	url = url.strip()
	adlist = requests.get(url)
	if adlist.status_code != 200:
		now = datetime.datetime.now()
		logging.error('{} Failed to get {} {}'.format(now.isoformat(), url, adlist.status_code))
	print('Processing {}'.format(url))
	return adlist.text

def read_list_file():
	with open(adlists_path) as adlists:
		return adlists.read()

def line_in_hashmap(line):
	_hash = hashlib.sha1(bytes(line, encoding="utf-8")).hexdigest()
	key = _hash[0:4]
	value = _hash[4:]
	if key not in hashmap.keys():
		hashmap[key] = [value]
		return True
	if key in hashmap.keys() and value not in hashmap[key]:
		hashmap[key].append(value)
		return False
	else:
		return True
	

def cleanup(adlist_text):
	with open(outfile, 'a+') as file:
		for line in adlist_text.split("\n"):
			line = re.sub('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} ', '', line)
			line = re.sub('#(.*)', '', line)
			line = re.sub('^<.*>$', '', line)
			if line and not line_in_hashmap(line):
				file.write(line+"\n")

if __name__ == '__main__':
	adlists = read_list_file()
	for url in adlists.split("\n"):
		adlist_text = get_adlist(url)
		cleanup(adlist_text)
